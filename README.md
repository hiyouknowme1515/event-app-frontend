Events App Frontend

[Link](https://eventfusionapp.netlify.app/)

Technology Used: ReactJs – frontend JS library, Bulma – CSS library

Specifications:	

 ` `User can create his/her account and list their events 
 They can delete their account  Update their post, delete their post
 On User Profile page their events which they have posted are also shown

Folder Structure

+---public

\---src

`    `+---auth

`    `+---core

`    `+---events

`    `\---users

- Public folder contains index.html page which has a div of id = root
- Auth folder contains authentication functions
- Core Folder contains Navbar and Home 
- Events folder contains Components of Events
- Users folder contains components of users


